//
//  ViewController.swift
//  DelegateExample
//
//  Created by Jason Henderson on 9/23/16.
//  Copyright © 2016 CSUMB. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, ChildViewControllerProtocol {

    func allDoneWithViewController(viewController: ChildViewController, message: String?) {
        viewController.dismissViewControllerAnimated(true) {
            if let text = message {
                print("\(text)")
            }
        }
    }

    @IBAction func openPressed(sender: AnyObject) {
        let childVC = self.storyboard?.instantiateViewControllerWithIdentifier("ChildVC") as! ChildViewController
        childVC.delegate = self
        self.presentViewController(childVC, animated:true, completion:nil)
    }

}

