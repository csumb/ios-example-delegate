//
//  ChildViewController.swift
//  DelegateExample
//
//  Created by Jason Henderson on 9/23/16.
//  Copyright © 2016 CSUMB. All rights reserved.
//

import UIKit

@objc protocol ChildViewControllerProtocol {
    optional func allDoneWithViewController(viewController: ChildViewController, message: String?)
}

public class ChildViewController: UIViewController {
    
    weak var delegate:ChildViewControllerProtocol?

     @IBAction func closePressed(sender: AnyObject) {
        self.delegate?.allDoneWithViewController?(self, message: "We are done here")
    }
}

